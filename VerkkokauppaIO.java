import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Luokkaa käytetään verkkokaupan tietojen tallentamiseen
 * ja lataamiseen tietovarastosta.
 *
 * @author Erkki
 */
public class VerkkokauppaIO {
    public static final String asiakasTiedosto = "asiakkaat.csv";
    public static String tuoteTiedosto = "tuotteet.csv";
    public static String myyjaTiedosto = "myyjat.csv";
    public static String tapahtumaTiedosto = "ostotapahtumat.csv";

    public static void main(String[] args) {
        // Tähän voi kirjoittaa koodia, jolla testata
        // kirjoitus- ja lukumetodien toimintaa helposti
    }

    private static final String EROTIN = ";";

    /**
     * @param tiedostonNimi tarkista löytyykö tiedosto, jos ei niin luo tyhjä tiedosto
     */
    public static void tarkistaJaLuoTiedosto(String tiedostonNimi) {
        File tiedosto = new File(tiedostonNimi);
        if (!tiedosto.exists()) {
            try {
                tiedosto.createNewFile();
            } catch (IOException e) {
                throw new RuntimeException("Virhe luotaessa tiedostoa: " + tiedostonNimi, e);
            }
        }
    }

    public static void kirjoitaTiedosto(String tiedostonNimi,
                                        String sisalto) {
        try (PrintWriter tiedosto = new PrintWriter(tiedostonNimi)) {
            tiedosto.write(sisalto);
        } catch (FileNotFoundException e) {
            System.out.println("Tapahtui virhe: " + e);
        }
    }

    /**
     * Lukee annetun nimisen tiedoston sisällön ja palauttaa sen listassa.
     * Jokainen listan alkio vastaa yhtä tiedoston riviä
     *
     * @param tiedostonNimi luettavan tiedoston nimi
     * @return tiedoston sisällön listana
     */
    public static ArrayList<String> lueTiedosto(String tiedostonNimi) {
        ArrayList<String> data = new ArrayList<>();
        try (Scanner lukija = new Scanner(new File(tiedostonNimi))) {
            while (lukija.hasNextLine()) {
                data.add(lukija.nextLine());
            }
        } catch (FileNotFoundException e) {
            System.out.println("Tapahtui virhe: " + e);
        }
        return data;
    }

    /**
     * Kirjoittaa asiakaslistan annetun nimiseen tiedostoon.
     */
    public static void kirjoitaAsiakkaat(ArrayList<Asiakas> asiakasLista) {
        String data = "";
        for (Asiakas asiakas : asiakasLista) {
            data += asiakas.getData(VerkkokauppaIO.EROTIN);
            data += "\n";
        }
        // Poistetaan viimeinen "turha" rivinvaihto
        if (data.length() > 0) {
            data = data.substring(0, data.length() - 1);
        }
        kirjoitaTiedosto(asiakasTiedosto, data);
    }

    /**
     * Palauttaa uuden asiakasolion annetun datarivin perusteella.
     * Rivillä tulee olla asiakasnumero, nimi ja tehdyt ostot tässä
     * järjestyksessä erotettuna merkillä
     * <code>VerkkokauppaIO.EROTIN</code>
     *
     * @param data datarivi, josta tiedot parsitaan
     * @return uuden KantaAsiakas tai Asiakas-olion dataan perustuen
     */
    public static Asiakas parsiAsiakas(String data) {
        String[] tiedot = data.split(VerkkokauppaIO.EROTIN);
        // Tässä vaiheessa tulee tietää tietojen järjestys
        String asNro = tiedot[0];
        String nimi = tiedot[1];
        double ostot = Double.parseDouble(tiedot[2]);

        // jos erotin köytää 4 datapistettä, on kyseessä kanta-asiakas
        if (tiedot.length == 4) {
            int alennusprosentti = Integer.parseInt(tiedot[3]);
            return new KantaAsiakas(asNro, nimi, ostot, alennusprosentti);
        }
        return new Asiakas(asNro, nimi, ostot);
    }

    /**
     * Metodi lukee asiakkaat annetun nimisestä tiedostosta ja
     * palauttaa sisällön mukaisen listan Asiakas-olioita.
     *
     * @return tiedostosta luetut asiakasoliot listana
     */
    public static ArrayList<Asiakas> lueAsiakkaat() {
        ArrayList<Asiakas> asiakkaat = new ArrayList<>();
        ArrayList<String> data = lueTiedosto(asiakasTiedosto);
        for (String adata : data) {
            Asiakas as = parsiAsiakas(adata);
            asiakkaat.add(as);
        }
        return asiakkaat;
    }

    /**
     * Kirjoittaa tuotelistan annetun nimiseen tiedostoon.
     *
     * @param tuotelista    lista tuotteista
     */
    public static void kirjoitaTuotteet(ArrayList<Tuote> tuotelista) {
        try (ObjectOutputStream oos =
                     new ObjectOutputStream(
                             new FileOutputStream(tuoteTiedosto))) {
            oos.writeObject(tuotelista);
        } catch (IOException e) {
            System.out.println("Tapahtui virhe: " + e);
        }
    }

    /**
     * Lukee tuotelistan tiedostosta
     *
     * @return listan tuotteita
     */
    public static ArrayList<Tuote> lueTuotteet() {
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(tuoteTiedosto))) {
            ArrayList<Tuote> tlista = (ArrayList<Tuote>) ois.readObject();
            return tlista;
        } catch (IOException e) {
            System.out.println("Tapahtui virhe: " + e);
        } catch (ClassNotFoundException e) {
            // Tämä virhe tulee, jos luettu tieto ei ole yhteensopiva
            // sen luokan kanssa, jonka tyyppiseksi se yritetään muuntaa
            System.out.println("Tapahtui virhe: " + e);
        }
        return new ArrayList<>();
    }

    /**
     * Kirjoittaa tiedot annetun nimiseen tiedostoon.
     * @param lista lista myyjistä
     */
    public static void kirjoitaMyyjat(ArrayList<Myyja> lista) {
        try (ObjectOutputStream oos =
                     new ObjectOutputStream(
                             new FileOutputStream(myyjaTiedosto))) {
            oos.writeObject(lista);
        } catch (IOException e) {
            System.out.println("Tapahtui virhe: " + e);
        }
    }

    /**
     * Lukee tiedostosta listan
     * @return lista myyjistä
     */
    public static ArrayList<Myyja> lueMyyjat() {
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(myyjaTiedosto))) {
            ArrayList<Myyja> lista = (ArrayList<Myyja>) ois.readObject();
            return lista;
        } catch (IOException e) {
            System.out.println("Tapahtui virhe: " + e);
        } catch (ClassNotFoundException e) {
            // Tämä virhe tulee, jos luettu tieto ei ole yhteensopiva
            // sen luokan kanssa, jonka tyyppiseksi se yritetään muuntaa
            System.out.println("Tapahtui virhe: " + e);
        }
        return new ArrayList<>();
    }

    /**
     * Kirjoittaa tiedot annetun nimiseen tiedostoon.
     * @param lista lista ostotapahtumista
     */
    public static void kirjoitaOstotapahtumat(ArrayList<Ostotapahtuma> lista) {
        try (ObjectOutputStream oos =
                     new ObjectOutputStream(
                             new FileOutputStream(tapahtumaTiedosto))) {
            oos.writeObject(lista);
        } catch (IOException e) {
            System.out.println("Tapahtui virhe: " + e);
        }
    }

    /**
     * Lukee tiedostosta listan
     * @return lista ostotapahtumista
     */
    public static ArrayList<Ostotapahtuma> lueOstotapahtumat() {
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(tapahtumaTiedosto))) {
            ArrayList<Ostotapahtuma> lista = (ArrayList<Ostotapahtuma>) ois.readObject();
            return lista;
        } catch (IOException e) {
            System.out.println("Tapahtui virhe: " + e);
        } catch (ClassNotFoundException e) {
            // Tämä virhe tulee, jos luettu tieto ei ole yhteensopiva
            // sen luokan kanssa, jonka tyyppiseksi se yritetään muuntaa
            System.out.println("Tapahtui virhe: " + e);
        }
        return new ArrayList<>();
    }
}
